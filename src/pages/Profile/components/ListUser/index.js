import React, { useEffect, useState } from "react";
import axios from "axios";
import ReactPaginate from "react-paginate";
import { Container } from "components";

function ListUser() {
  const [isDataUser, setDataUser] = useState([]);
  const [offset, setOffset] = useState(0);
  const [perPage] = useState(10);
  const [pageCount, setPageCount] = useState(0);

  useEffect(() => {
    getData();
  }, [offset]);

  const getData = async () => {
    const res = await axios.get(`https://randomuser.me/api/?results=20`);
    const data = res.data.results;
    console.log("data", res);
    const slice = data.slice(offset, offset + perPage);

    const postData = slice.map((item, index) => (
      <div
        key={index}
        className="box-list-user d-inline-block  shadow bg-body rounded "
      >
        <div className="d-flex align-items-center ">
          <div className="box-images-user">
            <img src={item.picture.large} className="w-100 h-100" alt="foto" />
          </div>
          <div>
            {dataDiri("Name", item.name.first + " " + item.name.last)}
            {dataDiri("Phone", item.phone)}
            {dataDiri("Email", item.email)}
            {dataDiri("Country", item.location.country)}
          </div>
        </div>
      </div>
    ));
    setDataUser(postData);
    setPageCount(Math.ceil(data.length / perPage));
  };
  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    setOffset(selectedPage + 1);
    window.scroll({
      top: 0,
      left: 0,
      behavior: "smooth",
    });
  };

  function dataDiri(label, value) {
    return (
      <div className="d-flex justify-content-between">
        <div className="content-user-left ">
          <p>{label}</p>
        </div>
        <div className="content-user-right">
          <p>{": " + value}</p>
        </div>
      </div>
    );
  }

  return (
    <Container title={"List User"}>
      <div className="">
        {isDataUser}
        <ReactPaginate
          previousLabel={"<<"}
          nextLabel={">>"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}
        />
      </div>
    </Container>
  );
}

export default ListUser;
