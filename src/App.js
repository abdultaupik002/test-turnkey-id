import React from "react";
import Profile from "pages/Profile";

import "assets/scss/style.scss";

function App() {
  return (
    <>
      <Profile />
    </>
  );
}

export default App;
