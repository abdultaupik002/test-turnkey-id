import React from "react";
import PropTypes from "prop-types";

function Container(props) {
  const { children, title } = props;
  return (
    <div className="container mt-2 d-flex flex-column">
      <p className="title-list-item mt-3 fs-1 fw-bold ">{title}</p>
      {children}
    </div>
  );
}
Container.defaultProps = {
  title: "Title",
};
Container.propTypes = { children: PropTypes.node, title: PropTypes.string };

export default Container;
